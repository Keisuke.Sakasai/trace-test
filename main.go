package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"sync"
	"time"

	"contrib.go.opencensus.io/exporter/jaeger"
	"go.opencensus.io/trace"

	"go.opencensus.io/plugin/ochttp"
	"go.opencensus.io/plugin/ochttp/propagation/tracecontext"

	"github.com/gorilla/mux"
)

func mainHandler(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Backend APP #2!\n")
	ch := make(chan context.Context, 2)
	ctx1 := r.Context()
	ctx2, _ := trace.StartSpan(ctx1, "/devide_span")
	ch <- ctx1
	ch <- ctx2

	requestPath1 := os.Getenv("requestPath1")
	requestPath2 := os.Getenv("requestPath2")
	sleepTime, _ := strconv.Atoi(os.Getenv("sleepTime"))
	outputLog := os.Getenv("outputLog")
	serviceName := os.Getenv("serviceName")

	if (requestPath1 != "") && (requestPath2 != "") {
		toServ1 := strings.Split(requestPath1, ".")[0]
		toServ2 := strings.Split(requestPath2, ".")[0]
		var wg sync.WaitGroup
		wg.Add(2)
		go func() {
			defer wg.Done()

			ctx1 := <-ch
			HTTPFormat1 := &tracecontext.HTTPFormat{}
			if spanContext, ok := HTTPFormat1.SpanContextFromRequest(r); ok {
				_, span := trace.StartSpanWithRemoteParent(ctx1, "service "+serviceName+" ==> "+toServ1, spanContext)
				defer span.End()
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("endpoint", requestPath1),
				}, "Call backend application.")

				time.Sleep(time.Duration(sleepTime) * time.Second)
				fmt.Println(sleepTime)

				req, err := http.NewRequest("GET", requestPath1, nil)
				if err != nil {
					log.Fatalf("%v", err)
				}
				childCtx, cancel := context.WithTimeout(req.Context(), 100000*time.Millisecond)
				defer cancel()
				req = req.WithContext(childCtx)
				format := &tracecontext.HTTPFormat{}
				format.SpanContextToRequest(span.SpanContext(), req)
				client := http.DefaultClient
				res, err := client.Do(req)
				if err != nil {
					log.Fatalf("%v", err)
				}
				fmt.Printf("%v\n", res.StatusCode)
			}
		}()
		fmt.Println(time.Now(), ":", requestPath1, " \n")

		go func() {
			defer wg.Done()
			ctx2 := <-ch
			HTTPFormat2 := &tracecontext.HTTPFormat{}
			if spanContext, ok := HTTPFormat2.SpanContextFromRequest(r); ok {
				_, span := trace.StartSpanWithRemoteParent(ctx2, "service "+serviceName+" ==> "+toServ2, spanContext)
				defer span.End()
				span.Annotate([]trace.Attribute{
					trace.StringAttribute("endpoint", requestPath2),
				}, "Call backend application.")

				time.Sleep(time.Duration(sleepTime) * time.Second)
				fmt.Println(sleepTime)

				req, err := http.NewRequest("GET", requestPath2, nil)
				if err != nil {
					log.Fatalf("%v", err)
				}
				childCtx, cancel := context.WithTimeout(req.Context(), 100000*time.Millisecond)
				defer cancel()
				req = req.WithContext(childCtx)
				format := &tracecontext.HTTPFormat{}
				format.SpanContextToRequest(span.SpanContext(), req)
				client := http.DefaultClient
				res, err := client.Do(req)
				if err != nil {
					log.Fatalf("%v", err)
				}
				fmt.Printf("%v\n", res.StatusCode)
			}
			fmt.Println(time.Now(), ":", requestPath2, " \n")
		}()

		wg.Wait()
		io.WriteString(w, outputLog)

	} else if (requestPath1 != "") && (requestPath2 == "") {
		toServ1 := strings.Split(requestPath1, ".")[0]
		ctx := r.Context()
		HTTPFormat := &tracecontext.HTTPFormat{}
		if spanContext, ok := HTTPFormat.SpanContextFromRequest(r); ok {
			_, span := trace.StartSpanWithRemoteParent(ctx, "service "+serviceName+" ==> "+toServ1, spanContext)
			defer span.End()
			span.Annotate([]trace.Attribute{
				trace.StringAttribute("endpoint", requestPath1),
			}, "Call backend application.")

			time.Sleep(time.Duration(sleepTime) * time.Second)

			req, err := http.NewRequest("GET", requestPath1, nil)
			if err != nil {
				log.Fatalf("%v", err)
			}
			childCtx, cancel := context.WithTimeout(req.Context(), 100000*time.Millisecond)
			defer cancel()
			req = req.WithContext(childCtx)
			format := &tracecontext.HTTPFormat{}
			format.SpanContextToRequest(span.SpanContext(), req)
			client := http.DefaultClient
			res, err := client.Do(req)
			if err != nil {
				log.Fatalf("%v", err)
			}
			fmt.Printf("%v\n", res.StatusCode)
		}
		fmt.Println(time.Now(), ":", requestPath1, " \n")

		io.WriteString(w, outputLog)

	} else if (requestPath1 == "") && (requestPath2 == "") {
		ctx := r.Context()
		HTTPFormat := &tracecontext.HTTPFormat{}
		if spanContext, ok := HTTPFormat.SpanContextFromRequest(r); ok {
			_, span := trace.StartSpanWithRemoteParent(ctx, "==> "+" service "+serviceName, spanContext)
			defer span.End()
			span.Annotate([]trace.Attribute{
				trace.StringAttribute("endpoint", "None"),
			}, "Call backend application.")

			time.Sleep(time.Duration(sleepTime) * time.Second)
			fmt.Println(sleepTime)

			req, err := http.NewRequest("GET", "http://localhost:8080", nil)
			if err != nil {
				log.Fatalf("%v", err)
			}

			client := http.DefaultClient
			res, err := client.Do(req)
			if err != nil {
				log.Fatalf("%v", err)
			}
			fmt.Printf("%v\n", res.StatusCode)
		}

		io.WriteString(w, outputLog)

	}
}

func main() {
	exporter, err := jaeger.NewExporter(jaeger.Options{
		AgentEndpoint: "jaeger-all-in-one-inmemory-agent:6831",
		Process: jaeger.Process{
			ServiceName: "Backend APP",
		},
	})
	if err != nil {
		log.Fatal(err)
	}
	defer exporter.Flush()

	trace.RegisterExporter(exporter)
	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})

	r := mux.NewRouter()
	r.HandleFunc("/", mainHandler)
	var handler http.Handler = r

	handler = &ochttp.Handler{
		Handler:     handler,
		Propagation: &tracecontext.HTTPFormat{}}

	log.Fatal(http.ListenAndServe(":8080", handler))
}
